//1. Get information from the Thanos boards
//2. Get all the lists for the Thanos board
//3. Get all cards for all lists simultaneously

const fs = require('fs');
const path = require('path');

const findBoardInformation = require('./callback1');
const findListBasedOnBoardId = require('./callback2');
const findCardBaseOnListId = require('./callback3');

function getInformationOfVillan(villianName) {
    setTimeout(() => {

        // read file and get id of villianName
        fs.readFile(path.join(__dirname, "boards.json"), (err, data) => {
            if (err) {
                console.log("error: ", err);
            } else {
                let boardData = JSON.parse(data);

                let villianId = boardData.filter((villian) => {
                    return villian.name == villianName;
                }).map((villianData) => {
                    return villianData.id;
                })

                villianId = villianId[0];

                //1. find villian detail and this function require from callback1.js 
                findBoardInformation(villianId, (err, villianDetails) => {
                    if (err) {
                        console.log("error: ", err);
                    } else {
                        if (villianDetails.length == 0) {
                            return [];
                        }
                        if (villianDetails[0].name == villianName) {
                            console.log(villianDetails);

                            //2. find data from lists.json
                            findListBasedOnBoardId(villianId, (err, powers) => {
                                if (err) {
                                    console.log("error: ", err);
                                } else {
                                    console.log(powers);

                                    if (powers.length > 0) {

                                        for (let index = 0; index < powers.length; index++) {

                                            // 3. find cards from cards.json based on power
                                            findCardBaseOnListId(powers[index].id, (err, cards) => {
                                                if (err) {
                                                    console.log(err);
                                                } else {

                                                    if (Array.isArray(cards)) {
                                                        console.log(powers[index].name, "details: ", cards);
                                                    } else {
                                                        console.log(powers[index].name, "details: ", {});
                                                    }
                                                   
                                                }
                                            });
                                        }
                                    }

                                }
                            });
                        }
                    }
                });
            }
        })
    });
}

module.exports = getInformationOfVillan;