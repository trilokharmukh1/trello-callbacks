// to find the board information based on board Id

const fs = require('fs');
const path = require('path');

function findBoardInformation(bId, callback) {
    setTimeout(() => {

        const readFile = (bId, callback) => {
            fs.readFile(path.join(__dirname, "boards.json"), (err, data) => {
                if (err) {
                    console.log("error: ", err);
                } else {
                    let list = JSON.parse(data);

                    let result = list.filter((board) => {
                        return board.id == bId;
                    })

                    callback(null, result);
                }
            })
        }

        return readFile(bId, callback);

    }, 2 * 1000);
}

module.exports = findBoardInformation;