//function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 

const fs = require('fs');
const path = require('path');

function findListBasedOnBoardId(bId, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, "lists.json"), (err, data) => {
            if (err) {
                console.log("error: ", err);
            } else {
                data = JSON.parse(data);
                let result = data[bId];
                
                callback(null, result);
            }
        })

    }, 2 * 1000);
}

module.exports = findListBasedOnBoardId;
