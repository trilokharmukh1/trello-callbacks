//1. Get information from the Thanos boards
//2. Get all the lists for the Thanos board
//3. Get all cards for the Mind and Space lists simultaneously

const fs = require('fs');
const path = require('path');

const findBoardInformation = require('./callback1');
const findListBasedOnBoardId = require('./callback2');
const findCardBaseOnListId = require('./callback3');

function detailsOfVillan(villianName, power) {

    setTimeout(() => {

        // read file and get id of villianName
        fs.readFile(path.join(__dirname, "boards.json"), (err, data) => {
            if (err) {
                console.log("error: ", err);
            } else {
                let boardData = JSON.parse(data);

                let villianId = boardData.filter((villian) => {
                    return villian.name == villianName;
                }).map((villianData) => {
                    return villianData.id;
                })

                villianId = villianId[0];

                //1. find villian detail and this function require from callback1.js 
                findBoardInformation(villianId, (err, villianDetails) => {
                    if (err) {
                        console.log("error: ", err);
                    } else {
                        if (villianDetails.length == 0) {
                            return [];
                        }
                        if (villianDetails[0].name == villianName) {
                            console.log(villianDetails);

                            //2. find data from lists.json
                            findListBasedOnBoardId(villianId, (err, result) => {
                                if (err) {
                                    console.log("error: ", err);
                                } else {
                                    console.log(result);
                                    // store power details
                                    if (Array.isArray(power)) {
                                        let powerId = result.filter((list) => {
                                            return power.includes(list.name);
                                        })

                                        if (powerId.length > 0) {

                                            for (let index = 0; index < powerId.length; index++) {

                                                // 3. find cards from cards.json based on power
                                                findCardBaseOnListId(powerId[index].id, (err, cards) => {
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        console.log(powerId[index].name, "power details: ", cards);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        })
    });
}

module.exports = detailsOfVillan;