//find all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json.

const fs = require('fs');
const path = require('path');

function findCardBaseOnListId(listId, callback) {
    setTimeout(() => {

        fs.readFile(path.join(__dirname, "cards.json"), (err, data) => {
            if (err) {
                console.log("error: ", err);
            } else {
                let fileData = JSON.parse(data);
                result = fileData[listId];

                callback(null, result);
            }
        })

    }, 2 * 1000);
}

module.exports = findCardBaseOnListId;