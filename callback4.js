// 1. Get information from the Thanos boards
// 2. Get all the lists for the Thanos board
// 3. Get all cards for the Mind list simultaneously
const fs = require('fs');
const path = require('path');

const findBoardInformation = require('./callback1');
const findListBasedOnBoardId = require('./callback2');
const findCardBaseOnListId = require('./callback3');

function findAllDetailsOfVillian(villianName) {

    setTimeout(() => {

        // read file and get id of villianName
        fs.readFile(path.join(__dirname, "boards.json"), (err, data) => {
            if (err) {
                console.log("error: ", err);
            } else {
                let boardData = JSON.parse(data);

                let villianId = boardData.filter((villian) => {
                    return villian.name == villianName;
                }).map((villianData) => {
                    return villianData.id;
                })

                villianId = villianId[0];

                //1. find villian detail and this function require from callback1.js 
                findBoardInformation(villianId, (err, villianDetails) => {
                    if (err) {
                        console.log("error: ", err);
                    } else {
                        if (villianDetails.length == 0) {
                            return [];
                        }
                        if (villianDetails[0].name == villianName) {

                            //2. find data from lists.json
                            findListBasedOnBoardId(villianId, (err, result) => {
                                if (err) {
                                    console.log("error: ", err);
                                } else {

                                    let mindId = result.filter((list) => {
                                        return list.name == "Mind";
                                    })
                                        .map((id) => {
                                            return id.id;
                                        })

                                    if (mindId.length > 0) {

                                        // 3. find cards from cards.json
                                        findCardBaseOnListId(mindId[0], (err, cards) => {
                                            if (err) {
                                                console.log(err);
                                            } else {

                                                console.log(villianDetails);
                                                console.log(result);
                                                console.log(cards);
                                            }
                                        });
                                    }
                                }
                            });

                        }
                    }
                });

            }
        })


    });

}

module.exports = findAllDetailsOfVillian;